#include "pch.h"
#include "Scheduler.h"
#include "JobFactory.h"
#include <chrono>

/*
 * Buesch Nadine <gs19m005>, Knaack Maximilian <gs19m003>, Schmidt Oliver <gs19m013>
 */

// Remotery is a easy to use profiler that can help you with seeing execution order and measuring data for your implementation
// Once initialized, you can use it by going into the "vis" folder of the project and opening "vis/index.html" in a browser (double-click)
// It emits 3 warnings at the moment, does can be ignored and will not count as warnings emitted by your code! ;)
// 
// Github: https://github.com/Celtoys/Remotery
#include "Remotery/Remotery.h"


#define MAKE_UPDATE_FUNC(NAME, DURATION) \
	void Update##NAME() { \
		rmt_ScopedCPUSample(NAME, 0); \
		auto start = std::chrono::high_resolution_clock::now(); \
		decltype(start) end; \
		do { \
			end = std::chrono::high_resolution_clock::now(); \
		} while (std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() < (DURATION)); \
	} \

// You can create other functions for testing purposes but those here need to run in your job system
// The dependencies are noted on the right side of the functions, the implementation should be able to set them up so they are not violated and run in that order!
MAKE_UPDATE_FUNC(Input, 200) // no dependencies
MAKE_UPDATE_FUNC(Physics, 1000) // depends on Input
MAKE_UPDATE_FUNC(Collision, 1200) // depends on Physics
MAKE_UPDATE_FUNC(Animation, 600) // depends on Collision
MAKE_UPDATE_FUNC(Particles, 800) // depends on Collision
MAKE_UPDATE_FUNC(GameElements, 2400) // depends on Physics
MAKE_UPDATE_FUNC(Rendering, 2000) // depends on Animation, Particles, GameElements
MAKE_UPDATE_FUNC(Sound, 1000) // no dependencies

void UpdateSerial()
{
	rmt_ScopedCPUSample(UpdateSerial, 0);
	UpdateInput();
	UpdatePhysics();
	UpdateCollision();
	UpdateAnimation();
	UpdateParticles();
	UpdateGameElements();
	UpdateRendering();
	UpdateSound();
}

// In `UpdateParallel` you should use your jobsystem to distribute the tasks
Scheduler scheduler;
JobFactory jobFactory;
void UpdateParallel()
{
	rmt_ScopedCPUSample(UpdateParallel, 0);

	auto start = std::chrono::high_resolution_clock::now();

	Job updateInput = jobFactory.CreateJob(UpdateInput);
	Job updatePhysics = jobFactory.CreateJob(UpdatePhysics, std::vector<int>{ updateInput.m_ID });
	Job updateCollision = jobFactory.CreateJob(UpdateCollision, std::vector<int>{ updatePhysics.m_ID });
	Job updateAnimation = jobFactory.CreateJob(UpdateAnimation, std::vector<int>{ updateCollision.m_ID });
	Job updateParticles = jobFactory.CreateJob(UpdateParticles, std::vector<int>{ updateCollision.m_ID });
	Job updateGameElements = jobFactory.CreateJob(UpdateGameElements, std::vector<int>{ updatePhysics.m_ID });
	Job updateRendering = jobFactory.CreateJob(UpdateRendering, std::vector<int>{ updateAnimation.m_ID, updateParticles.m_ID, updateGameElements.m_ID });
	Job updateSound = jobFactory.CreateJob(UpdateSound);

	scheduler.AddJob(updateInput);
	scheduler.AddJob(updatePhysics);
	scheduler.AddJob(updateCollision);
	scheduler.AddJob(updateAnimation);
	scheduler.AddJob(updateParticles);
	scheduler.AddJob(updateGameElements);
	scheduler.AddJob(updateRendering);
	scheduler.AddJob(updateSound);

	scheduler.DoUntilDone();

	auto end = std::chrono::high_resolution_clock::now();
	std::cout << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << std::endl;
}

int main()
{
	/*
	 * This initializes remotery, you are not forced to use it (although it's helpful)
	 * but please also don't remove it from here then. Because if you don't use it, I
	 * will most likely do so, to track how your jobs are issued and if the dependencies run
	 * properly
	 */
	Remotery* rmt;
	rmt_CreateGlobalInstance(&rmt);

	std::atomic<bool> isRunning = true;

	std::thread serial([&isRunning]()
	{
		while (isRunning)
			UpdateSerial();
	});

	std::thread parallel([&isRunning]()
	{
		while (isRunning)
			UpdateParallel();
	});

	std::cout << "Type anything to quit...\n";
	char c;
	std::cin >> c;
	std::cout << "Quitting...\n";
	isRunning = false;

	serial.join();
	parallel.join();

	rmt_DestroyGlobalInstance(rmt);
}

