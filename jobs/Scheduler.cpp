#include "pch.h"
#include "Scheduler.h"

// SCHEDULER +++++++++++++++++++++++++++++++++++++++++
Scheduler::Scheduler() {
	CreateThreads();
}

void Scheduler::AddJob(Job job) {
	m_queueAll.push_back(job);
	m_dependencies.push_back(job.m_ID);
}

void Scheduler::SetNumThreads(unsigned int numThreads) {
	m_numThreads = numThreads;

	m_isRunning = false;	//indicator to finish current execution
	m_readyCV.notify_all();

	for (auto& t : m_threads) { //join all currently existing threads
		t.join();
	}

	m_threads.clear();	//delete old threads
	CreateThreads();	//and finally create specified number of threads
}

void Scheduler::CreateThreads() {
	if (m_numThreads <= 0) { //if no specific number of threads was specified, we check for the ideal amount
		m_numThreads = static_cast<int>(std::thread::hardware_concurrency());
		if (m_numThreads > 1) { //to make sure we do not create 0 threads on a system with only 1 cpu
			--m_numThreads; //we create one thread less than ideal to acount for our main thread
							//otherwise we want as many threads as we can run simultaneously for maximum efficiency
							//in our case we don't want any more, as we want to prevent bouncing and our threads (once started)
							//do not need to wait for anything, so switching threads would not yield any advantage
		}
	}

	m_isRunning = true;
	for (int n = 0; n < m_numThreads; ++n)
		m_threads.push_back(std::thread([this] { RunThread(); }));

}

//the main function for the threads
void Scheduler::RunThread() {
	while (m_isRunning) {
		bool job_valid = false;
		Job j;
		{
			std::unique_lock<std::mutex> lock(m_readyMutex);
			m_readyCV.wait(lock);				//when notified...
			if (!m_queueReady.empty()) {//get a job from the ready queue back
				j = m_queueReady.back();
				m_queueReady.pop_back();
				job_valid = true;
			}
		}
		if (job_valid) {
			j.Execute();	//execute job

			std::unique_lock<std::mutex> lock(m_dependencyMutex);
			auto it = std::find(m_dependencies.begin(), m_dependencies.end(), j.m_ID);
			m_dependencies.erase(it);	//remove the currently executed job from the dependencies after locking
		}
	}
}

//the main function for our scheduling routine
void Scheduler::DoUntilDone() {
	while (true) {	//executed until all jobs dependencies could be fulfilled and jobs could be queued for execution
		{
			std::unique_lock<std::mutex> lock(m_dependencyMutex);
			if (m_dependencies.empty()) {
				return;
			}
		}
		std::vector<int> deletionList;
		for (int j = 0; j < m_queueAll.size(); ++j) {
			bool can_run = true;
			if (!m_queueAll.at(j).m_jobDependencies.empty()) { //as long as there are jobs in the all_queue
				for (auto d : m_queueAll.at(j).m_jobDependencies) {//check job dependencies
					std::unique_lock<std::mutex> lock(m_dependencyMutex);
					auto it = std::find(m_dependencies.begin(), m_dependencies.end(), d);
					if (it != m_dependencies.end()) {//if one of the dependencies of the job is still listed in m_dependencies, it can't be executed yet
						can_run = false;
						break;
					}
				}
			}
			if(can_run) {	//if all job dependencies are fulfilled...
				{
					std::unique_lock<std::mutex> lock(m_readyMutex);//...push it to the ready_queue...
					m_queueReady.push_front(m_queueAll.at(j));
				}
				m_readyCV.notify_one();	//...and notify one of our waiting threads
				deletionList.push_back(j); //note that the job should be removed from all_queue
			}
		}
		for (int counter = deletionList.size() - 1; counter >= 0; --counter) {
			m_queueAll.erase(m_queueAll.begin() + deletionList[counter]); //update the all_queue after each iteration
		}
	}
}
