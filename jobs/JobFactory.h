#pragma once
#include"Job.h"

// jobfactory creates jobs and assigns them their unique id
struct JobFactory {

	Job CreateJob(std::function<void()> function);
	Job CreateJob(std::function<void()> function, std::vector<int> dependencies);

	std::atomic<int> m_ID = 0;
};
