#include "pch.h"
#include "Job.h"

// JOB +++++++++++++++++++++++++++++++++++++++++
void Job::Execute() {
	if (m_ID >= 0) {
		m_function();
	}
}