#pragma once

#include "Job.h"
#include <deque>

// the scheduler class contains all the logic for the jobsystem
// it creates the specified number of threads or, if none given, checks how many threads can be run on the current machine
// the scheduler adds threads to the all_job_queue
// while there are jobs in the all_job_queue, the scheduler checks if they can be moved to the ready_job_queue
// if a job has its dependencies fulfilled it is moved to the ready_job_queue to be executed by one of the threads
// whenever a job is pushed to the ready_job_queue, one thread is notified through a condition variable
// whenever a job is done, its unique id is removed from the dependencies
// all acesses to the queues are guarded through a mutex, as well as the dependencies vector
struct Scheduler {
	Scheduler();

	void SetNumThreads(unsigned int numThreads);

	void CreateThreads();

	void RunThread();

	void AddJob(Job job);

	void DoUntilDone();


	bool m_isRunning = false;
	int m_numThreads;
	std::vector<std::thread> m_threads;
	std::vector<Job> m_queueAll;

	std::mutex m_dependencyMutex;
	std::vector<int> m_dependencies;

	std::mutex m_readyMutex;
	std::condition_variable m_readyCV;
	std::deque<Job> m_queueReady;
};
