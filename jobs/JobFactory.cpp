#include "pch.h"
#include "JobFactory.h"

// JOBFACTORY +++++++++++++++++++++++++++++++++++++++++
Job JobFactory::CreateJob(std::function<void()> function) {
	return Job(m_ID++, function);
}

Job JobFactory::CreateJob(std::function<void()> function, std::vector<int> dependencies) {
	return Job(m_ID++, function, dependencies);
}