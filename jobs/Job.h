#pragma once

#include "pch.h"
#include <vector>
#include <functional>

// A job has a function, it can execute, and knows its dependencies
struct Job {
	Job() : m_ID(-1) {}
	Job(int ID, std::function<void()> function) : m_ID(ID), m_function(function) {}
	Job(int ID, std::function<void()> function, std::vector<int> dependencies) : m_ID(ID), m_function(function), m_jobDependencies(dependencies) {}


	void Execute();


	int m_ID;
	std::vector<int> m_jobDependencies;
	std::function<void()> m_function;
};